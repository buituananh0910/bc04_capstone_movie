import React from "react";
import Carousel from "../Components/Carousel/Carousel";
import Header from "../Components/HeaderTheme/Header";
import ListMovie from "../Components/ListMovie/ListMovie";
import Intro from "../intro/Intro";
import { movieServ } from "../Services/movieServices";

import TabsMovies from "../Pages/HomePage/TabsMovies";
import Footer from "../Components/FooterTheme/Footer";

export default function Layout({ Component }) {
  // let [danhSachPhim, setDanhSachPhim] = useState([]);
  // useEffect(() => {
  //   movieServ
  //     .layDanhSachPhim()
  //     .then((result) => {
  //       if (result) {
  //         setTimeout(() => {
  //           setDanhSachPhim(result.data);
  //         }, 1500);
  //       }
  //     })
  //     .catch((err) => {
  //       console.log(err.response.data);
  //     });
  // }, []);
  return (
    <div className=" bg-black ">
      <Header />
      {<Component />}
      <br />
      <br />
      <Footer />
    </div>
  );
}
