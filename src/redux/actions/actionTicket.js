import { SET_TICKET } from "../constants/constantsTicket";

export const setTicket = (payload) => {
  return {
    type: SET_TICKET,
    payload,
  };
};
