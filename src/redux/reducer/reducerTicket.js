import { localServ } from "../../Services/localService";
import { DAT_VE, SET_TICKET } from "../constants/constantsTicket";
let initialState = {
  ticketInfor: {},
  danhSachGheDangDat: [],
};
export const ticketReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TICKET: {
      state.ticketInfor = action.payload;
      return { ...state };
    }
    case DAT_VE: {
      let danhSachGheCapNhat = state.danhSachGheDangDat;
      let index = danhSachGheCapNhat.findIndex(
        (gheDD) => gheDD.maGhe === action.payload.maGhe
      );
      if (index != -1) {
        danhSachGheCapNhat.splice(index, 1);
      } else {
        danhSachGheCapNhat.push(action.payload);
      }
      state.danhSachGheDangDat = danhSachGheCapNhat;
      console.log(state.danhSachGheDangDat);
      return { ...state };
    }
    default:
      return state;
  }
};
