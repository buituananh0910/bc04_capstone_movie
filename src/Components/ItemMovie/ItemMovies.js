import React from "react";
import { NavLink } from "react-router-dom";

import "./homeItem.css";

export default function ItemMovies({ data = {}, children }) {
  const [open, setOpen] = React.useState(false);
  const handleToggle = () => setOpen(!open);
  return (
    <div className="item__movie">
      <div className="item__link">
        <div className="item__img">
          <img src={data.hinhAnh} alt={data.hinhAnh} />
          <div className="overlay">
            <div className="play__button" style={{ cursor: "pointer" }}>
              <i className="fa fa-play play__icon" />
            </div>
          </div>
          <span className="film__age age--C">{data.maNhom}</span>
          <span className="film__audit">
            <p className="film__point">8</p>
            <i className="fa fa-star film__star" />
            <i className="fa fa-star film__star" />
            <i className="fa fa-star film__star" />
            <i className="fa fa-star film__star" />
          </span>
        </div>
        <div className="item__info">
          <p className="film__name">{data.tenPhim}</p>
          <div className="item__button">
            <NavLink
              className="btn buyTicket__button"
              to={`/detail/${data.maPhim}`}
            >
              Chi Tiết Phim
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}
