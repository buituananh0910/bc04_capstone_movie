import React from "react";
import "./Footer.css";

import {
  AndroidFilled,
  AppleFilled,
  FacebookFilled,
  InstagramFilled,
} from "@ant-design/icons";
export default function Footer() {
  return (
    //
    <div className="p-5 " style={{ backgroundColor: "#222", height: "250px" }}>
      <div className="container mx-auto">
        <div className="grid grid-cols-3 gap-3 text-white ">
          <div className="flex">
            <div>
              <p style={{ marginBottom: "5px" }}>TIX</p>
              <div className="flex flex-col">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  FAQ
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Brand
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Guidelines
                </a>
              </div>
            </div>
            <div className="flex flex-col">
              <a
                className="col-6 col-lg-12"
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.cgv.vn/"
              >
                Thỏa thuận sử dụng
              </a>
              <a
                className="col-6 col-lg-12"
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.cgv.vn/"
              >
                Chính sách bảo mật
              </a>
            </div>
          </div>

          <div className="">
            <div>
              Đối tác
              <div className="grid grid-cols-5 gap-3">
                <a
                  style={{ width: "20px", height: "20px" }}
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  CGV
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  BHD
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Galaxycine
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Cinestar
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Lotte
                </a>
              </div>
              <div className="grid grid-cols-5 gap-3">
                <a
                  style={{ width: "20px", height: "20px" }}
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Megags
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Beta
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  DDCinema
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  TouchCinema
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.cgv.vn/"
                >
                  Cinemax
                </a>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-20">
            <div className="text-center px-10 ">
              MOBIL APP
              <div className="flex justify-between">
                <a href="https://www.cgv.vn/">
                  <AppleFilled
                    style={{ fontSize: "50px", cursor: "pointer" }}
                  />
                </a>
                <a href="https://www.cgv.vn/">
                  <AndroidFilled
                    style={{ fontSize: "50px", cursor: "pointer" }}
                  />
                </a>
              </div>
            </div>
            <div className="text-center px-10 ">
              SOCIAL APP
              <div className="flex justify-between">
                <a href="https://www.cgv.vn/">
                  <FacebookFilled
                    style={{ fontSize: "50px", cursor: "pointer" }}
                  />
                </a>

                <a href="https://www.cgv.vn/">
                  <InstagramFilled
                    style={{ fontSize: "50px", cursor: "pointer" }}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
        <p className="text-white mt-5 text-center">
          TIX - SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZIONĐịa chỉ: Z06 Đường số 13,
          Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.Giấy chứng
          nhận đăng ký kinh doanh số: 0101659783,đăng ký thay đổi lần thứ 30,
          ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí
          Minh cấp.Số Điện Thoại (Hotline): 1900 545 436
          <br />
          Email: support@tix.vn
        </p>
      </div>
    </div>
  );
}
