import React, { Fragment, useEffect, useState } from "react";

import { movieServ } from "../../Services/movieServices";

import ItemMovies from "../ItemMovie/ItemMovies";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css/navigation";
import { EffectCoverflow, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/bundle";
import "./itemMovie.css";
import { useDispatch } from "react-redux";
import {
  setLoadingOffAction,
  setLoadingOnAction,
} from "../../redux/actions/actionSpinner";
import Carousel from "../Carousel/Carousel";
import TabsMovies from "../../Pages/HomePage/TabsMovies";
import Footer from "../FooterTheme/Footer";
export default function HomePage() {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOnAction());
    movieServ
      .getListMovie()
      .then((res) => {
        dispatch(setLoadingOffAction());
        console.log(res);
        setMovies(res.data.content);
      })
      .catch((err) => {
        dispatch(setLoadingOffAction());
        console.log(err);
      });
  }, []);

  const renderMovies = () => {
    return (
      <Swiper
        effect={"coverflow"}
        grabCursor={true}
        centeredSlides={true}
        slidesPerView={"auto"}
        coverflowEffect={{
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        }}
        pagination={true}
        modules={[EffectCoverflow, Pagination]}
        className="mySwiper"
      >
        {movies.map((data, index) => {
          return (
            <SwiperSlide key={index}>
              <ItemMovies data={data} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    );
  };
  return (
    <Fragment>
      <div className="container mx-auto space-y-10">
        <Carousel movies={movies} />
        <div className="gap-10">{renderMovies()}</div>
        <TabsMovies className="space-x-2" />
      </div>
    </Fragment>
  );
}
