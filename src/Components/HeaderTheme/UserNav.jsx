import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  console.log("user: ", user);

  let handleLogout = () => {
    localServ.user.remove();
    window.location.href = `/login`;
  };
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="text-white">{user.hoTen}</span>
          <button
            onClick={handleLogout}
            className="text-white border rounded border-blue-500 px-5 px-1.5
          hover:bg-slate-300 hover:text-slate-700
          transition"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="border rounded border-red-500 text-red-400 px-5 px-1.5">
              Đăng Nhập
            </button>
          </NavLink>
          <NavLink to="/register">
            <button className="border rounded border-green-500 text-green-400 px-5 px-1.5">
              Đăng Ký
            </button>
          </NavLink>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
