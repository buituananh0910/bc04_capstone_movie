import { Button, Form, Input, message } from "antd";
import { userServ } from "../../Services/userServices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import React from "react";
import Lottie from "lottie-react";
import bg_animate from "../../assets/animation/45722-rocket-loader.json";
import { setUserLoginActionServ } from "../../redux/actions/actionUser";
const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userServ.postLogin(values);
    let onSuccess = () => {
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate(-1);
      }, 1000);
    };
    let onFail = () => {
      message.error("Đăng Nhập thất bại");
    };
    dispatch(setUserLoginActionServ(values, onSuccess, onFail));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div
      style={{
        backgroundImage: `url(
    "https://img.freepik.com/free-vector/cinema-cartoon-horizontal-banners_1284-9193.jpg?w=900&t=st=1666068439~exp=1666069039~hmac=207544d258700fa8c6fd4bd3b1044bbc021766506b331c4b01917e71a35764c5"
  )`,
        backgroundRepeat: "no-repeat",
        overflow: "hidden",
        backgroundSize: "cover",
      }}
    >
      <div
        className="container mx-auto h-screen w-screen flex items-center 
    justify-center"
      >
        <div className="w-1/2 h-full">
          <Lottie animationData={bg_animate} />
        </div>
        <div className="w-1/2 h-full flex items-center justify-center">
          <Form
            style={{
              backgroundColor: "#fff",
              width: "50%",
              padding: "20px",
              borderRadius: "10px",
            }}
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{}}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Login
            </h1>
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Button type="primary" htmlType="submit">
                  Đăng Nhập
                </Button>
                <Button
                  type="primary"
                  className="mt-10"
                  onClick={() => {
                    navigate("/register");
                  }}
                >
                  Đăng Ký
                </Button>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
