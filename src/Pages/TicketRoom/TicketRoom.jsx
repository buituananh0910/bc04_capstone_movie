import { message } from "antd";
import _ from "lodash";
import React from "react";
import { Fragment } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../../Components/FooterTheme/Footer";
import { setTicket } from "../../redux/actions/actionTicket";
import { DAT_VE } from "../../redux/constants/constantsTicket";
import { localServ } from "../../Services/localService";
import { movieServ } from "../../Services/movieServices";
import "./TicketRoom.css";
export default function TicketRoom() {
  const [thongTinPhim, setThongTinPhim] = useState([]);
  const [danhSachGhe, setDanhSachGhe] = useState([]);
  const { userInfor } = useSelector((state) => state.userReducer);
  const [isSuccess, setIsSuccess] = useState(false);
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let { maLichChieu } = useParams();
  let isLogin = localServ.user.get();
  useEffect(() => {
    if (!isLogin) {
      navigate("/login");
    } else {
      movieServ
        .layDanhSachPhongVe(maLichChieu)
        .then((res) => {
          setThongTinPhim(res.data.content.thongTinPhim);
          setDanhSachGhe(res.data.content.danhSachGhe);
          dispatch(setTicket(res.data.content.danhSachGhe));
          setIsSuccess(true);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, []);
  const { ticketInfor, danhSachGheDangDat } = useSelector(
    (state) => state.ticketReducer
  );
  let dataDatVe = {
    maLichChieu: maLichChieu,
    danhSachVe: danhSachGheDangDat,
  };
  const renderSeats = () => {
    return ticketInfor.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      if (ghe.daDat === true) {
        if (ghe.taiKhoanNguoiDat === isLogin.taiKhoan) {
          classGheDaDat = "gheDaDuocDat";
        } else {
          classGheDaDat = "gheDaDat";
        }
      }
      let classGheDangDat = "";
      let indexgheDD = danhSachGheDangDat.findIndex(
        (gheDD) => ghe.maGhe === gheDD.maGhe
      );
      if (indexgheDD != -1) {
        classGheDangDat = "gheDangDat";
      }
      return (
        <Fragment>
          <button
            disabled={classGheDaDat}
            key={index}
            className={`ghe  ${classGheVip} ${classGheDaDat} ${classGheDangDat} `}
            onClick={() => {
              dispatch({
                type: DAT_VE,
                payload: ghe,
              });
            }}
          >
            {ghe.stt}
          </button>
          {(index + 1) % 16 == 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };
  return (
    <Fragment>
      <div className="container mx-auto min-h-screen p-5">
        <div className="grid grid-cols-12">
          <div className="col-span-8 min-h-screen mr-20">
            <div
              style={{ background: "black", width: "100%", height: "20px" }}
            ></div>
            <div className="hinh_thang text-center mb-10">
              <p>Màn Hình</p>
            </div>
            <div className="text-center">{isSuccess ? renderSeats() : ""}</div>
            <div className="flex justify-between mt-10">
              <p>
                Ghế Đã Đặt
                <div className="ghe gheDaDat"></div>
              </p>
              <p>
                Ghế Vip
                <div className="ghe gheVip"></div>
              </p>
              <p>
                Ghế Thường
                <div className="ghe gheThuong"></div>
              </p>
              <p>
                Ghế Đang Đặt
                <div className="ghe gheDangDat"></div>
              </p>
              <p>
                Ghế Của Người Dùng Đặt
                <div className="ghe gheDaDuocDat"></div>
              </p>
            </div>
          </div>

          <div className="col-span-4 text-xl min-h-screen ">
            <h3 className="text-center text-4xl text-green-400">
              {" "}
              <span className="text-green-500 ">
                {danhSachGheDangDat
                  .reduce((tongTien, gheDD, index) => {
                    return (tongTien += gheDD.giaVe);
                  }, 0)
                  .toLocaleString()}
                đ
              </span>
            </h3>
            <hr />
            <h3 className="text-2xl">{thongTinPhim.tenPhim}</h3>
            <p>Địa điểm: {thongTinPhim.diaChi}</p>
            <p>
              Ngày Chiếu: {thongTinPhim.ngayChieu} - Giờ Chiếu:{" "}
              {thongTinPhim.gioChieu} {thongTinPhim.tenRap}
            </p>
            <hr />
            <div className="flex justify-between my-3">
              <div>
                <span className="text-red-500">Ghế : </span>
                {_.sortBy(danhSachGheDangDat, ["stt"]).map((gheDD, index) => {
                  return (
                    <span key={index} className="text-xl text-green-500 mr-2">
                      {gheDD.stt}
                    </span>
                  );
                })}
              </div>
              <div>
                <span className="text-green-500 ">
                  {danhSachGheDangDat
                    .reduce((tongTien, gheDD, index) => {
                      return (tongTien += gheDD.giaVe);
                    }, 0)
                    .toLocaleString()}
                  đ
                </span>
              </div>
            </div>
            <hr />
            <div className="my-3">
              <i>Email : </i>
              <br />
              {userInfor?.email}
            </div>
            <hr />
            <div className="my-3">
              <i>Phone : </i>
              <br />
              {userInfor?.soDT}
            </div>
            <br />

            <div
              onClick={() => {
                movieServ
                  .postDatVe(dataDatVe)
                  .then((res) => {
                    console.log(res.data.content);
                    message.success("Đặt Vé Thành Công");
                    setTimeout(() => {
                      navigate(0);
                    }, 1000);
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              }}
              className="mb-0 text-center   bg-green-400 text-white p-5 cursor-pointer"
            >
              Đặt Vé
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </Fragment>
  );
}
