import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Footer from "../../Components/FooterTheme/Footer";
import Header from "../../Components/HeaderTheme/Header";
import { movieServ } from "../../Services/movieServices";
import TabsDetailMovie from "./TabsDetailMovie";
export default function DetailMovie() {
  const { id } = useParams();
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .layThongTinPhim(id)
      .then((res) => {
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let {
    biDanh,
    dangChieu,
    danhGia,
    hinhAnh,
    hot,
    maNhom,
    maPhim,
    moTa,
    ngayKhoiChieu,
    sapChieu,
    tenPhim,
    trailer,
  } = dataMovies;
  // console.log("id", id);
  return (
    <div className="bg-black">
      <Header />
      <div
        style={{
          backgroundImage:
            "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjcbD_-rFqY5pRmd7WpLeE8ycVQ9ZY0DHF007KHwhKOtZTKOsCoSD4QB-HN0X1xvhFOKw&usqp=CAU)",
          minHeight: "100vh",
          minWidth: "100vw",
        }}
        href="#"
        className="flex flex-col items-center bg-white rounded-lg  shadow-md md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700"
      >
        <div
          style={{
            width: "30%",
            height: "80vh",
            marginLeft: "20px",
            background: "white",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="flex"
        >
          <img
            style={{ width: "95%", height: "95%" }}
            className="object-cover "
            src={hinhAnh}
            alt=""
          />
        </div>
        <div className="flex flex-col justify-between p-4 leading-normal text-white text-2xl">
          <h2 className="text-white text-6xl">{tenPhim}</h2>
          <p className=" text-3xl">{moTa}</p>
          <p>Đánh Giá : {danhGia}</p>
          <p>Mã Nhóm : {maNhom}</p>
          <p>Mã Phim : {maPhim}</p>
          <p>Ngày Khởi Chiếu : {ngayKhoiChieu}</p>
          <div>
            <a href={trailer}>
              <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mr-5">
                Xem Trailer
              </button>
            </a>
            {/* <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
              Đặt Vé Ngay
            </button> */}
          </div>
        </div>
      </div>
      <TabsDetailMovie id={id} />
      <br />
      <br />
      <Footer />
    </div>
  );
}
